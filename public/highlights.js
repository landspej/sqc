document.querySelectorAll('.footnote-link').forEach(link => {
    link.addEventListener('click', function() {
        const footnoteId = this.getAttribute('href').substring(1);
        const footnote = document.getElementById(footnoteId);

        // Remove highlight from all other footnotes
        document.querySelectorAll('.highlighted').forEach(el => {
            el.classList.remove('highlighted');
        });

        // Add highlight to the selected footnote
        if (footnote) {
            footnote.classList.add('highlighted');

            setTimeout(() => {
                footnote.classList.remove('highlighted');
            }, 3000);
        }
    });
});
