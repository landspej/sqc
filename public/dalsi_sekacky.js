function duplicateElement(element, target, count) {
    for (let i = 0; i < count; i++) {
        // Clone the element
        let clone = element.cloneNode(true);
        // Append the cloned element to the target
        target.appendChild(clone);
    }
}

function createAnchorElement() {
    let anchor = document.createElement("a");
    anchor.href = "sekacka.html";
    anchor.className = "product-item";

    let img = document.createElement("img");
    img.src = "products/DSC01397.jpg";
    img.alt = "Sekačka";
    anchor.appendChild(img);

    let h3 = document.createElement("h3");
    h3.textContent = "Sekačka";
    anchor.appendChild(h3);

    let price = document.createElement("p");
    price.textContent = "$TBD";
    anchor.appendChild(price);

    let description = document.createElement("p");
    description.textContent = "Krátký popisek";
    anchor.appendChild(description);

    return anchor;
}

let elementToDuplicate = createAnchorElement();
let targetDiv = document.getElementById('product-container');

// To číslo lze upravit na počet sekaček, který tam chceš navíc
duplicateElement(elementToDuplicate, targetDiv, 0);
